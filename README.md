# Common PHP Image for projects
Features:
- multiple PHP versions
- build-in common extensions:
  - PDO Mysql & SqLite
  - ZIP
  - GD
  - XDEBUG (activated by env variable)
- custom memory limt

Supported PHP versions:
- 8.0 - 8.3
- 7.3 - 7.4

## Quick start
To download image with PHP 8.3 run
```
docker run -it registry.gitlab.com/flyingarmageddon/php-base-image:8.3 /bin/bash
```

To enable xdebug add `env` variable:
```
docker run -it -e FLYING_PHP_XDEBUG_ENABLED=1 registry.gitlab.com/flyingarmageddon/php-base-image:8.1 /bin/bash
```
Message `Enabling xdebug...` should appear in console

## Env Variables
- `FLYING_PHP_XDEBUG_ENABLED` - set 1 to enable xdebug, default `0`
- `FLYING_PHP_MEMORY_LIMIT` - set PHP memory limit, default `512M`

## Building
To build image with latest supported PHP version run
```
make build
```
To build image with specified php version ex. `7.4` run:
```
PHP_VERSION=7.4 XDEBUG_VERSION=3.1.6 make build
```
Remember to match xdebug version to php. For now our builds use:
- 3.2.0 for PHP 8.0+
- 3.1.6 for PHP 7.x (latest supported)

To test build and quick run you can use
```
make run-it
```
for latest supported PHP version or
```
PHP_VERSION=7.3 make run-it
```
for specified version. You can also add `XDEBUG_ENABLED=1` tu enable xdebug inside container