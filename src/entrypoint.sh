#!/bin/bash
set -euo pipefail

USER_ID=${USER_ID:-$(id -u www-data)}

if [[ "${FLYING_PHP_XDEBUG_ENABLED}" == "1" ]]; then
	echo "Enabling xdebug..."
	docker-php-ext-enable xdebug
	ln -s /usr/local/flying/php/10-xdebug.ini /usr/local/etc/php/conf.d/
	echo "Done"
fi

echo "Setting memory limit..."
echo "memory_limit = ${FLYING_PHP_MEMORY_LIMIT}" > /usr/local/etc/php/conf.d/10-memory-limit.ini
echo "Done"

echo "Running command '${1}'"
bash -c $1