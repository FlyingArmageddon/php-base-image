ARG ARG_PHP_VERSION=8.3
ARG ARG_IMAGE_VERSION=dev
ARG ARG_XDEBUG_VERSION=3.3.1

FROM php:${ARG_PHP_VERSION}-alpine AS compiler

ARG ARG_XDEBUG_VERSION

COPY ./src /tmp/installer

WORKDIR /tmp/installer
RUN apk add --no-cache \
		bash \
		autoconf \
		gcc \
		libc-dev \
		make \
		git \
		sqlite-dev \
		libzip-dev \
		libexif-dev \
		gd-dev \
		linux-headers
		# && \
RUN docker-php-ext-install \
		exif \
		gd \
		pdo_mysql \
		pdo_sqlite \
		zip
		# && \
RUN	pecl install xdebug-${ARG_XDEBUG_VERSION}
	# docker-php-ext-disable xdebug && \
RUN	/tmp/installer/install_composer.sh


FROM php:${ARG_PHP_VERSION}-fpm-alpine

ARG ARG_PHP_VERSION
ARG ARG_IMAGE_VERSION

ENV FLYING_IMAGE_VERSION=${ARG_IMAGE_VERSION}
ENV FLYING_PHP_VERSION=${ARG_PHP_VERSION}
ENV FLYING_PHP_XDEBUG_ENABLED=0
ENV FLYING_PHP_MEMORY_LIMIT="512M"

COPY --from=compiler \
	/usr/local/etc/php/conf.d \
	/usr/local/etc/php/conf.d
COPY --from=compiler \
	/usr/local/lib/php/extensions \
	/usr/local/lib/php/extensions
COPY --from=compiler \
	/tmp/installer/composer.phar \
	/usr/bin/composer
COPY config /usr/local/flying
COPY src/entrypoint.sh /usr/bin/entrypoint
RUN apk add --no-cache \
		bash \
		curl \
		libgd \
		libzip \
		su-exec && \
	ln -s $(which su-exec) /usr/bin/gosu && \
	ln -s /usr/local/flying/php/10-fpm-user.ini /usr/local/etc/php-fpm.d/zzzz-10-fpm-user.ini

ENTRYPOINT [ "/usr/bin/entrypoint" ]
CMD ["php-fpm -F -R"]
