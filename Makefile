.PHONY: build run-it

IMAGE_TAG?=dev
PHP_VERSION?=8.2
XDEBUG_VERSION?=3.3.1
build:
	docker build \
		--no-cache \
		-t flying-php:$(IMAGE_TAG) \
		--build-arg ARG_PHP_VERSION=$(PHP_VERSION) \
		--build-arg ARG_XDEBUG_VERSION=$(XDEBUG_VERSION) \
	.

run-it:
	docker run \
		-v $(PWD)/src:/tmp/dev \
		-v $(PWD)/config/php/10-fpm-user.ini:/usr/local/etc/php-fpm.d/zzzz-10-fpm-user.conf \
		-e=FLYING_PHP_XDEBUG_ENABLED=$(XDEBUG_ENABLED) \
		-e=USER_ID=$(USER_ID) \
		--entrypoint /tmp/dev/entrypoint.sh \
		-it flying-php:$(IMAGE_TAG) /bin/sh